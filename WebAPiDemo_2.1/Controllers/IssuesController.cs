﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebAPiDemo_2._1.Models;

namespace WebAPiDemo_2._1.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class IssuesController : ControllerBase
    {
        private readonly IIssueRepository _issueRepository;
        private readonly ILogger _logger;

        public IssuesController(IIssueRepository issueRepository, ILogger<IssuesController> logger)
        {
            _issueRepository = issueRepository;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            _logger.LogInformation(LoggingEvents.GetItem, "Get all items");
            var issues = await Task.Run(() => _issueRepository.GetAll());      
            return Ok(issues);
        }

        [HttpGet("{key}")]
        public async Task<IActionResult> Get([FromRoute]int? key)
        {
            _logger.LogInformation(LoggingEvents.GetItem, $"Get item {key}");
            if (key == null)
            {
                _logger.LogWarning(LoggingEvents.GetItemInvalidInput, $"Get item {key} INVALID INPUT");
                return BadRequest();
            }

            int nonNullKey = key ?? default(int);
            var issue = await Task.Run(() => _issueRepository.Get(nonNullKey));

            if (issue == null)
            {
                _logger.LogWarning(LoggingEvents.GetItemNotFound, $"Get item {key} NOT FOUND");
                return NotFound();
            }
            return Ok(issue);
        }


        [HttpDelete("{key}")]
        public async Task<IActionResult> Delete([FromRoute] int? key)
        {
            int nonNullKey = key ?? default(int);
            var issue = await Task.Run(() => _issueRepository.Get(nonNullKey));

            if (key == null )
            {
                _logger.LogWarning(LoggingEvents.DeleteItemInvalidInput, $"Get item {key} in Delete INVALID INPUT");
                return BadRequest();
            }

            if (issue == null)
            {
                _logger.LogWarning(LoggingEvents.GetItemNotFound, $"Delete item {key} NOT FOUND");
                return NotFound();
            }

            var response =await Task.Run(() => _issueRepository.Delete(nonNullKey));
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Issue issue)
        {
            _logger.LogInformation(LoggingEvents.InsertItem, "Insert item");
            await Task.Run(() => _issueRepository.Add(issue));
            var response = await Task.Run(() => _issueRepository.GetAll());
            return Ok(response);
        }

        [HttpPatch("{key}")]
        public async Task<IActionResult> Update([FromRoute] int? key, [FromBody] Issue issue)
        {
            _logger.LogInformation(LoggingEvents.UpdateItem, $"Update item {key}");
            int nonNullKey = key ?? default(int);
            var getIssue = await Task.Run(() => _issueRepository.Get(nonNullKey));

            //no argument passed in
            if (issue == null || key == null)
            {
                _logger.LogWarning(LoggingEvents.UpdateItemInvalidInput, $"Update item {key} INVALID INPUT");
                return BadRequest();
            }

            //did not pass in Id, no issue found
            if (getIssue == null)
            {
                _logger.LogWarning(LoggingEvents.GetItemNotFound, $"Update item {key} NOT FOUND");
                return NotFound();
            }
            //check the item if it is already existed
            var response = await Task.Run(() => _issueRepository.Update(nonNullKey, issue));
            return Ok(response);
        }
    }
}