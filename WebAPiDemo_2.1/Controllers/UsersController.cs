﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebAPiDemo_2._1.Models;
using WebAPiDemo_2._1.Services;

namespace WebAPiDemo_2._1.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUserRepository _userRepository;
        private readonly ILogger _logger;

        public UsersController(IUserRepository userRepository, ILogger<UsersController> logger)
        {
            _userRepository = userRepository;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] User userParam)
        {
            _logger.LogInformation(LoggingEvents.Login, $"User {userParam.Username}  attempts login.");
            var user = await Task.Run(()=> _userRepository.Authenticate(userParam.Username, userParam.Password));

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            _logger.LogInformation(LoggingEvents.Login, $"User {userParam.Username}  logged in succesfully.");
            return Ok(user);
        }

    }
}