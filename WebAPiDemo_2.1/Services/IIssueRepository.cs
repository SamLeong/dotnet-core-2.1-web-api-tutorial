﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPiDemo_2._1.Models
{
    public interface IIssueRepository
    {
        Issue Get(int Id);
        List<Issue> GetAll();
        List<Issue> Delete(int Id);
        List<Issue> Add(Issue issue);
        List<Issue> Update(int Id, Issue issue);
    }
}
