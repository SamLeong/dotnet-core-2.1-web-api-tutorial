﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPiDemo_2._1.Models
{
    public class IssueRepository : IIssueRepository
    {
        private List<Issue> _issueList;

        public IssueRepository()
        {
            _issueList = new List<Issue>()
            {
                new Issue() {Id =1, Description = "Issue1"},
                new Issue() {Id =2, Description = "Issue2"},
                new Issue() {Id =3, Description = "Issue3"},
                new Issue() {Id =4, Description = "Issue4"},
                new Issue() {Id =5, Description = "Issue5"},
            };
        }

        public List<Issue> Delete(int Id)
        {
            var issue = _issueList.Where(x => x.Id == Id).FirstOrDefault();
            _issueList.Remove(issue);
            return _issueList;
        }

        public List<Issue> GetAll()
        {
            return _issueList;
        }

        public Issue Get(int Id)
        {
            var issue = _issueList.FirstOrDefault(x => x.Id == Id);
            return issue;
        }

        public List<Issue> Add(Issue issue)
        {
            _issueList.Add(issue);
            return _issueList;
        }

        public List<Issue> Update(int Id, Issue updateIssue)
        {
            var issue = _issueList.FirstOrDefault(x => x.Id == Id);
            issue.Id = updateIssue.Id;
            issue.Description = updateIssue.Description;          
            return _issueList;
        }
    }
}
