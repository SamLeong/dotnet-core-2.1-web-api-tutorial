﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPiDemo_2._1.Models;

namespace WebAPiDemo_2._1.Services
{
    public interface IUserRepository
    {
        User Authenticate(string username, string password);
    }
}
