﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebAPiDemo_2._1.Helpers;
using WebAPiDemo_2._1.Models;

namespace WebAPiDemo_2._1.Services
{
    public class UserRepository : IUserRepository
    {

        private List<User> _users = new List<User>()
        {
           new User{ FirstName = "Sam", Id = 1, LastName = "Watson", Password ="12345678", Username ="Admin", Role ="Admin"},
           new User{ FirstName = "David", Id = 1, LastName = "Oswald", Password ="12345678", Username ="Manager", Role ="Super"}
        };

        private readonly AppSettings _appSettings;
        public UserRepository(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public User Authenticate(string username, string password)
        {
            var user = _users.SingleOrDefault(x => x.Username == username && x.Password == password);

            if (user == null)
                return null;

            //generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptorr = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.Role)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)

            };

            var token = tokenHandler.CreateToken(tokenDescriptorr);
            user.Token = tokenHandler.WriteToken(token);
            user.Password = null;

            return user;
            }




        public IEnumerable<User> GetAll()
        {
            throw new NotImplementedException();
        }
    }    
}
