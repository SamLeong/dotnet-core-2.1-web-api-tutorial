﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using WebAPiDemo_2._1.Controllers;
using WebAPiDemo_2._1.Helpers;
using WebAPiDemo_2._1.Models;
using WebAPiDemo_2._1.Services;
using Xunit;

namespace WebApiDemoTest
{
    public class UsersControllerTest
    {
        UsersController _userController;
        IUserRepository _iuserRepository;
        ILogger<UsersController> _logger = new Logger<UsersController>(new NullLoggerFactory());//pass an empty instance to ilogger

        public UsersControllerTest()
        {
            var appSettings = Options.Create(new AppSettings() {Secret = "thisismydevelopmentSecret!@#" });
            _iuserRepository = new UserRepository(appSettings);
            _userController = new UsersController(_iuserRepository, _logger);
        }

        [Fact]
        public void Authenticate_WhenCalled_ReturnUser()
        {
            //Arrange
            User user = new User() { Username = "Manager", Password = "12345678" };

            //Act
            var okResult = _userController.Authenticate(user);

            //Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public void Authenticate_WhenCalled_ReturnBadRequestMessage()
        {
            //Arrange
            User user = new User() { Username = "Manager", Password = "1234!@#$" };

            //Act
            var badResult = _userController.Authenticate(user);

            //Assert
            Assert.IsType<BadRequestObjectResult>(badResult.Result);
        }
    }
}
