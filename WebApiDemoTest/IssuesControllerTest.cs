using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using System;
using WebAPiDemo_2._1.Controllers;
using WebAPiDemo_2._1.Models;
using Xunit;
namespace WebApiDemoTest
{
    /// <summary>
    /// General order - Arrange -> Act -> Assert
    /// </summary>
    public class IssuesControllerTest
    {
        IssuesController _issuesController;
        IIssueRepository _iissueRepository; 
        ILogger<IssuesController> _logger = new Logger<IssuesController>(new NullLoggerFactory());//pass an empty instance to ilogger

        public IssuesControllerTest()
        {
            _iissueRepository = new IssueRepositoryService();
            _issuesController = new IssuesController(_iissueRepository, _logger);
        }

        [Fact]
        public void GetAll_WhenCalled_ReturnAllIssues()
        {
            //Act
            var okResult = _issuesController.GetAll();

            //Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public void GetById_ExistingIdPassed_ReturnFoundIssue()
        {
            //Arrange
            int key = 2;

            //Act
            var okResult = _issuesController.Get(key);

            //Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public void GetById_NonExistingIdPassed_ReturnNotFound()
        {
            //Arrange
            int key = -1;

            //Act
            var okResult = _issuesController.Get(key);

            //Assert
            Assert.IsType<NotFoundResult>(okResult.Result);
        }

        [Fact]
        public void GetById_NullIdPassed_ReturnBadRequest()
        {
            //Arrange
            int? key = null;

            //Act
            var badResult = _issuesController.Get(key);

            //Assert
            Assert.IsType<BadRequestResult>(badResult.Result);
        }

        [Fact]
        public void DeleteById_NullIdPassed_ReturnBadRequest()
        {
            //Arrange
            int? key = null;

            //Act
            var badResult = _issuesController.Get(key);

            //Assert
            Assert.IsType<BadRequestResult>(badResult.Result);
        }

        [Fact]
        public void DeleteById_ExistingIdPassed_ReturnAllIssues()
        {
            //Arrange
            int? key = 2;

            //Act
            var okResult = _issuesController.Delete(key);

            //Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public void DeleteById_NonExistingIdPassed_ReturnNotFound()
        {
            //Arrange
            int? key = -1;

            //Act
            var badResult = _issuesController.Delete(key);

            //Assert
            Assert.IsType<NotFoundResult>(badResult.Result);
        }

        [Fact]
        public void Create_ValidIssuePassed_ReturnAllItems()
        {
            //Arrange
            Issue issue = new Issue() { Id = 201, Description ="New issue from unit test"};

            //Act
            var okResult = _issuesController.Create(issue);

            //Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public void UpdateById_ExistingIdPassed_ReturnAllIssues()
        {
            //Arrange
            int? key = 4;
            Issue issue = new Issue() { Id = 301, Description = "Updated issue from unit test" };
            //Act
            var okResult = _issuesController.Update(key, issue);

            //Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public void UpdateById_NullIdPassed_ReturnBadRequest()
        {
            //Arrange
            int? key = null;
            Issue issue = new Issue() { Id = 301, Description = "Updated issue from unit test" };

            //Act
            var badResult = _issuesController.Update(key, issue);

            //Assert
            Assert.IsType<BadRequestResult>(badResult.Result);
        }

        [Fact]
        public void UpdateById_NonExistingIdPassed_ReturnNotFound()
        {
            //Arrange
            int? key = -1;
            Issue issue = new Issue() { Id = 301, Description = "Updated issue from unit test" };

            //Act
            var badResult = _issuesController.Update(key, issue);

            //Assert
            Assert.IsType<NotFoundResult>(badResult.Result);
        }

    }
}
